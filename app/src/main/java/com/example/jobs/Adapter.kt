package com.example.jobs

import android.graphics.ColorSpace
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.custom_layout.view.*
import java.util.*
import kotlin.collections.ArrayList

class Adapter: RecyclerView.Adapter<Adapter.MyViewHolder>(), Filterable {

    //Lista com valores
    var list = mutableListOf<String>("Bruno", "Jessica", "Candice")

    //Lista vazia
    var countryFilterList: MutableList<String>

    //Lista recebe lista com valores
    init {
        countryFilterList = list
    }

    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    //Carrega o layout custom_layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter.MyViewHolder {
       return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_layout,parent,false))
    }

    //Obtem tamanho da lista
    override fun getItemCount(): Int {
        return countryFilterList.size
    }

    override fun onBindViewHolder(holder: Adapter.MyViewHolder, position: Int) {
        holder.itemView.textViewNome.text = countryFilterList[position].toString()
    }

    /*
    Metodo responsavel por fazer o filtro de pesquisa e atualizar o recyclerview
     */
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    countryFilterList = list
                } else {
                    val resultList = ArrayList<String>()
                    for (row in list) {
                        if (row.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            resultList.add(row)
                        }
                    }
                    countryFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = countryFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                countryFilterList = results?.values as ArrayList<String>
                notifyDataSetChanged()
            }
        }
    }
}